const csvToJSON = require("../util");
const fs = require("fs");

function matchesWonPerYearPerTeam() {
  try {
    csvToJSON("../data/matches.csv").then((data) => {
      //intializing the count
      let count = {};
      //iterating data
      for (let index = 0; index < data.length; index++) {
        if (!count[data[index].winner]) {
          count[data[index].winner] = {};
          if (!count[data[index].winner][data[index].season]) {
            count[data[index].winner][data[index].season] = 1;
          } else {
            count[data[index].winner][data[index].season] += 1;
          }
        } else {
          if (!count[data[index].winner][data[index].season]) {
            count[data[index].winner][data[index].season] = 1;
          } else {
            count[data[index].winner][data[index].season] += 1;
          }
        }
      }
      console.log(count);

      fs.writeFile(
        "../public/output/matches_won_per_year_team.json",
        JSON.stringify(count),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully getting output");
          }
        }
      );
    });
  } catch (err) {
    console.log("some thing went wrong" + err);
  }
}

matchesWonPerYearPerTeam();
