const csvToJSON = require("../util");
const fs = require("fs");

const deliveryPath = "../data/deliveries.csv";
const matchesPath = "../data/matches.csv";

function getTop10EconomicalBowlersIn2015() {
  try {
    csvToJSON(matchesPath).then((matchesData) => {
      csvToJSON(deliveryPath).then((deliveryData) => {
        let totalBalls = {};
        let totalRuns = {};

        for (
          let outerIndex = 0;
          outerIndex < matchesData.length;
          outerIndex++
        ) {
          if (matchesData[outerIndex].season == 2015) {
            for (let index = 0; index < deliveryData.length; index++) {
              if (matchesData[outerIndex].id == deliveryData[index].match_id) {
                if (!totalBalls[deliveryData[index].bowler]) {
                  totalBalls[deliveryData[index].bowler] = 0;
                  totalRuns[deliveryData[index].bowler] = 0;
                }
                totalBalls[deliveryData[index].bowler] += 1;
                totalRuns[deliveryData[index].bowler] += Number(
                  deliveryData[index].total_runs
                );
              }
            }
          }
        }
        let bowlerEconomy = [];
        for (const key in totalBalls) {
          let temp = {};
          let runs = totalRuns[key];
          let overs = totalBalls[key] / 6;
          temp["name"] = key;
          temp["economy"] = runs / overs;
          bowlerEconomy.push(temp);
        }
        // console.log(bowlerEconomy)

        for (
          let outerIndex = 0;
          outerIndex < bowlerEconomy.length - 1;
          outerIndex++
        ) {
          for (
            let innerIndex = outerIndex + 1;
            innerIndex < bowlerEconomy.length;
            innerIndex++
          ) {
            let value = bowlerEconomy[outerIndex].economy;

            let value1 = bowlerEconomy[innerIndex].economy;
            if (value > value1) {
              let temp = bowlerEconomy[outerIndex];
              bowlerEconomy[outerIndex] = bowlerEconomy[innerIndex];
              bowlerEconomy[innerIndex] = temp;
            }
          }
        }
        console.log(bowlerEconomy.slice(0, 10));

        fs.writeFile(
          "../public/output/top_10_econamic_bowler_in_2015.json",
          JSON.stringify(bowlerEconomy.slice(0, 10)),
          (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("successfully getting output");
            }
          }
        );
      });
    });
  } catch (err) {
    console.log("some thing went wrong" + err);
  }
}

getTop10EconomicalBowlersIn2015();
