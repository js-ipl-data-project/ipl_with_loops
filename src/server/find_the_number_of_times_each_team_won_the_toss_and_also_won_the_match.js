const csvToJSON=require("../util")
const fs=require("fs")

const matchesPath="../data/matches.csv";
function getNumberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch(){
    csvToJSON(matchesPath).then((matchesData)=>{
        let count={}
        for(let index=0;index<matchesData.length;index++){
            if(matchesData[index].toss_winner==matchesData[index].winner){
                  if(!count[matchesData[index].toss_winner]){
                    count[matchesData[index].toss_winner]=1
                  }else{
                  count[matchesData[index].toss_winner] +=1
                  }
            }
        }
        console.log(count)
        fs.writeFile("../public/output/no_of_time_each_team_won_the_toss_and_also_won_match.json",JSON.stringify(count),(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("successfully getting output")
            }
        })
    })
}
getNumberOfTimesEachTeamWonTheTossAndAlsoWonTheMatch()