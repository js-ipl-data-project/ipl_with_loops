const csvToJSON = require("../util");
const fs = require("fs");

const deliveryPath = "../data/deliveries.csv";

function getBestEconomyBowlerInSuperOver() {
  try {
    csvToJSON(deliveryPath).then((deliveryData) => {
      let balls = {};
      let runs = {};
      for (let index = 0; index < deliveryData.length; index++) {
        let data = deliveryData[index];
        if (data.is_super_over == 1) {
          if (!balls[data.bowler]) {
            //console.log(data.bowler)
            runs[data.bowler] = Number(data.total_runs);
            balls[data.bowler] = 1;
          } else {
            runs[data.bowler] += Number(data.total_runs);
            balls[data.bowler] += 1;
          }
        }
      }
      // console.log(balls)

      let bowlerEconomy = [];
      for (const key in balls) {
        let temp = {};
        let totalRuns = runs[key];
        //console.log(totalRuns)
        // console.log(balls)
        let overs = balls[key] / 6;
        // console.log(overs)
        temp["name"] = key;
        temp["economy"] = totalRuns / overs;
        bowlerEconomy.push(temp);
      }

      //console.log(bowlerEconomy)
      for (
        let outerIndex = 0;
        outerIndex < bowlerEconomy.length - 1;
        outerIndex++
      ) {
        for (
          let innerIndex = outerIndex + 1;
          innerIndex < bowlerEconomy.length;
          innerIndex++
        ) {
          let value = bowlerEconomy[outerIndex].economy;

          let value1 = bowlerEconomy[innerIndex].economy;
          if (value > value1) {
            let temp = bowlerEconomy[outerIndex];
            bowlerEconomy[outerIndex] = bowlerEconomy[innerIndex];
            bowlerEconomy[innerIndex] = temp;
          }
        }
      }
      console.log(bowlerEconomy.slice(0, 1));
      fs.writeFile(
        "../public/output/best_bowler_economy_in_superOver.json",
        JSON.stringify(bowlerEconomy.slice(0, 1)),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully getting output");
          }
        }
      );
    });
  } catch (err) {
    console.log(err);
  }
}

getBestEconomyBowlerInSuperOver();
