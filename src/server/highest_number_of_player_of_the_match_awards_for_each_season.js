const csvToJSON=require("../util")
const fs=require('fs');

const matchesPath="../data/matches.csv";

function getHighestNumberOfPlayerOfTheMatchAwardsForEachSeason(){
    csvToJSON(matchesPath).then((matchesData)=>{
        let highestPlayerOfTheMatch={};

     for(let index=0;index<matchesData.length;index++){
         if(!highestPlayerOfTheMatch[matchesData[index].season]){
            highestPlayerOfTheMatch[matchesData[index].season]={}
                    if(!highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match]){
                        highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match]=1
                    }else{
                        highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match] +=1
                    }
                    
                   }else{
                    if(!highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match]){

                        highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match]=1
                    }else{
                        highestPlayerOfTheMatch[matchesData[index].season][matchesData[index].player_of_match] +=1
                    }
                   }

                   
                }
                let result ={}
                    for (const season in highestPlayerOfTheMatch) {
                        for (const player in highestPlayerOfTheMatch[season]) {
                            let awards=highestPlayerOfTheMatch[season][player]
                           if (!result[season] || result[season].awards<awards) {
                                result[season]={player,awards}
                           }
                        } 
                    }
                    console.log(result)
              //  console.log(highestPlayerOfTheMatch)
                fs.writeFile("../public/output/highest_no_of_player_of_the_mathes_award_each_season.json",JSON.stringify(result),(err)=>{
                    if(err){
                        console.log(err);
                    }else{
                        console.log("successfully getting output")
                    }
                    
                })
    })
}

getHighestNumberOfPlayerOfTheMatchAwardsForEachSeason()
