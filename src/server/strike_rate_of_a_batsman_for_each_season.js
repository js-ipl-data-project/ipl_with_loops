const csvToJSON = require("../util");
const fs = require("fs");

const deliveryPath = "../data/deliveries.csv";
const matchesPath = "../data/matches.csv";

function getStrikeRateOfBatsman(batsman) {
  try {
    csvToJSON(matchesPath).then((matchesData) => {
      csvToJSON(deliveryPath).then((deliveryData) => {
        let strikeRateOfBatsman = {};

        for (let index = 0; index < matchesData.length; index++) {
             //checking if the season previously there are not if not there intializing 
          if (!strikeRateOfBatsman[matchesData[index].season]) {
            strikeRateOfBatsman[matchesData[index].season] = {
              runs: 0,
              balls: 0,
            };
          } //if season there add the runs and balls batsman
          else {
            for (
              let innerIndex = 0;
              innerIndex < deliveryData.length;
              innerIndex++
            ) {
              if (matchesData[index].id == deliveryData[innerIndex].match_id) {
                if (batsman == deliveryData[innerIndex].batsman) {
                  strikeRateOfBatsman[matchesData[index].season]["runs"] +=
                    Number(deliveryData[innerIndex].batsman_runs);
                  strikeRateOfBatsman[matchesData[index].season]["balls"] += 1;
                }
              }
            }
          }
        }
        // console.log(strikeRateOfBatsman)
        let seasons = {};
        //calculating the strike rate
        for (const season in strikeRateOfBatsman) {
          let totalRuns = strikeRateOfBatsman[season].runs;
          let totalBalls = strikeRateOfBatsman[season].balls;
          let strikeRate = (totalRuns / totalBalls) * 100;
          seasons[season] = strikeRate;
        }
        console.log(seasons);
        fs.writeFile( "../public/output/strike_rate_batsman_for_each_season.json",JSON.stringify(seasons),
          (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("successfully getting output");
            }
          }
        );
      });
    });
  } catch {
    console.log("some thing went wrong" + err);
  }
}

getStrikeRateOfBatsman("S Dhawan");
