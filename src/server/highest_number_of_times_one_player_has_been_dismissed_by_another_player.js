const csvToJSON = require("../util");
const fs = require("fs");

const deliveryPath = "../data/deliveries.csv";

function getHighestNoOfOnePlayerHasDismissedByAnotherPlayer(playerName) {
  try {
    csvToJSON(deliveryPath).then((deliveriesData) => {
      let highestPlayerDismissed = {};
      for (let index = 0; index < deliveriesData.length; index++) {
        if (playerName == deliveriesData[index].player_dismissed) {
          if (!highestPlayerDismissed[deliveriesData[index].bowler]) {
            highestPlayerDismissed[deliveriesData[index].bowler] = 1;
          } else {
            highestPlayerDismissed[deliveriesData[index].bowler] += 1;
          }
        }
      }
      // console.log(highestPlayerDismissed)

      let bowlerName = Object.entries(highestPlayerDismissed);
      //console.log(bowlerName)
      for (let outerIndex = 0; outerIndex < bowlerName.length; outerIndex++) {
        for (let innerIndex = 0; innerIndex < bowlerName.length; innerIndex++) {
          if (bowlerName[outerIndex][1] > bowlerName[innerIndex][1]) {
            let temp = bowlerName[innerIndex];
            bowlerName[innerIndex] = bowlerName[outerIndex];
            bowlerName[outerIndex] = temp;
          }
        }
      }
      console.log(bowlerName[1]);
      fs.writeFile(
        "../public/output/highest_no_of_player_has_dismissed.json",
        JSON.stringify(bowlerName[1]),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully getting output");
          }
        }
      );
    });
  } catch (err) {
    console.log("something went wrong " + err);
  }
}

getHighestNoOfOnePlayerHasDismissedByAnotherPlayer("SR Watson");
