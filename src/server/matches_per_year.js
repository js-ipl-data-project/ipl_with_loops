const csvToJSON = require("../util");
const fs = require("fs");

function matchesPerYear() {
  try {
    csvToJSON("../data/matches.csv").then((data) => {
      //intializing the count
      let count = {};
      //iterting the data
      for (let index = 0; index < data.length; index++) {
        // console.log(data)
        // check the data in count present or not
        if (!count[data[index].season]) {
          count[data[index].season] = 0;
        }
        count[data[index].season] += 1;
      }
      console.log(count);

      //it is creating file and storing output
      fs.writeFile(
        "../public/output/matches_per_year.json",
        JSON.stringify(count),
        (err) => {
          if (err) {
            console.log(err);
          } else {
            console.log("successfully getting output");
          }
        }
      );
    });
  } catch {
    console.log("something went wrong" + err);
  }
}

console.log(matchesPerYear());
