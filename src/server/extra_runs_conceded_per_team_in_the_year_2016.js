const csvToJSON = require("../util");
const fs = require("fs");

const deliveryPath = "../data/deliveries.csv";
const matchesPath = "../data/matches.csv";

function extraRunsConcededPerTeamInTheYear2016() {
  try {
    csvToJSON(matchesPath).then((matchesData) => {
      csvToJSON(deliveryPath).then((deliveryData) => {
        let extraRuns = {};
        for (let index = 0; index < matchesData.length; index++) {
          if (matchesData[index].season == 2016) {
            for (
              let innerIndex = 0;
              innerIndex < deliveryData.length;
              innerIndex++
            ) {
                //calculating extra runs each team conceded
              if (matchesData[index].id === deliveryData[innerIndex].match_id) {
                if (!extraRuns[deliveryData[innerIndex].bowling_team]) {
                  extraRuns[deliveryData[innerIndex].bowling_team] = 1;
                }
                extraRuns[deliveryData[innerIndex].bowling_team] += parseInt(
                  deliveryData[innerIndex].extra_runs
                );
              }
            }
          }
        }
        console.log(extraRuns);

        fs.writeFile(
          "../public/output/extra_runs_conceded_per_team_in_the_year_2016.json",
          JSON.stringify(extraRuns),
          (err) => {
            if (err) {
              console.log(err);
            } else {
              console.log("successfully getting output");
            }
          }
        );
      });
    });
  } catch {
    console.log("some thing went wrong" + err);
  }
}

console.log(extraRunsConcededPerTeamInTheYear2016());
